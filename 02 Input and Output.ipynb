{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Input and Output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this chapter we'll be looking at the functionality that Biopython provides for reading and writing files. This is all handled by the `Bio.SeqIO` module so let's start by importing it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from Bio import SeqIO"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Files for this chapter\n",
    "\n",
    "For this section you need to download some files which we will be reading in. You can either download them by hand from using these links: [V01408.1.fasta](https://milliams.com/courses/biopython/V01408.1.fasta), [ls_orchid_short.gbk](https://milliams.com/courses/biopython/ls_orchid_short.fasta) and [ls_orchid.gbk](https://milliams.com/courses/biopython/ls_orchid.gbk) or run the following Python code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import urllib\n",
    "for f in [\"V01408.1.fasta\", \"ls_orchid_short.fasta\", \"ls_orchid.gbk\"]:\n",
    "    urllib.request.urlretrieve(f\"https://milliams.com/courses/biopython/{f}\", f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `SeqRecord` objects\n",
    "\n",
    "If you have a FASTA file with a single sequence in it, the simplest way to read it in is with `SeqIO.read`. This takes two arguments, the name of the file you want to open, and the format that file is in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ID: ENA|V01408|V01408.1\n",
      "Name: ENA|V01408|V01408.1\n",
      "Description: ENA|V01408|V01408.1 Tobacco mosaic virus genome (variant 1)\n",
      "Number of features: 0\n",
      "Seq('GTATTTTTACAACAATTACCAACAACAACAAACAACAAACAACATTACAATTAC...CCA')\n"
     ]
    }
   ],
   "source": [
    "tm = SeqIO.read(\"V01408.1.fasta\", \"fasta\")\n",
    "print(tm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `read` functions returns an object called a `SeqRecord`. These are like the `Seq`s that we saw before but have additional information associated with them. A full list of these are:\n",
    "\n",
    "<dl>\n",
    "<dt><code>.seq</code></dt>\n",
    "    <dd>The sequence itself, typically a <code>Seq</code> object as we saw in the last chapter.</dd>\n",
    "    \n",
    "<dt><code>.id</code></dt>\n",
    "    <dd>The primary ID used to identify the sequence.</dd>\n",
    "\n",
    "<dt><code>.name</code></dt>\n",
    "    <dd>A \"common\" name/id for the sequence. In some cases this will be the same as the ID, but it could also be a clone name.</dd>\n",
    "\n",
    "<dt><code>.description</code></dt>\n",
    "    <dd>A human readable description or expressive name for the sequence.</dd>\n",
    "\n",
    "<dt><code>.letter_annotations</code></dt>\n",
    "    <dd>\n",
    "        Holds per-letter-annotations using a (restricted) dictionary of additional information about the letters in the sequence.\n",
    "        The keys are the name of the information, and the information is contained in the value as a Python sequence (i.e. a list, tuple or string) with the same length as the sequence itself.\n",
    "        This is often used for quality scores or secondary structure information (e.g. from alignment files).\n",
    "    </dd>\n",
    "\n",
    "<dt><code>.annotations</code></dt>\n",
    "    <dd>\n",
    "        A dictionary of additional information about the sequence.\n",
    "        The keys are the name of the information, and the information is contained in the value.\n",
    "        This allows the addition of more \"unstructured\" information to the sequence.\n",
    "    </dd>\n",
    "\n",
    "<dt><code>.features</code></dt>\n",
    "    <dd>A list of <code>SeqFeature</code> objects with more structured information about the features on a sequence (e.g. position of genes on a genome, or domains on a protein sequence).</dd>\n",
    "    \n",
    "<dt><code>.dbxrefs</code></dt>\n",
    "    <dd>A list of database cross-references as strings.</dd>\n",
    "</dl>\n",
    "\n",
    "So you can get at the `Seq` object fom inside the record with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Seq('GTATTTTTACAACAATTACCAACAACAACAAACAACAAACAACATTACAATTAC...CCA')"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "tm.seq"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Have a look at the value of some of these attributes of the `tm` object. Which are available and which are missing or empty?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading in multiple sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is very common to have files which contain multiple sequences. Biopython provides an interface to read these in, one after another which is the `SeqIO.parse` function. This provides you with an object which you can loop over with a `for` loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "gi|2765658|emb|Z78533.1|CIZ78533 C.irapeanum 5.8S rRNA gene and ITS1 and ITS2 DNA\n",
      "gi|2765657|emb|Z78532.1|CCZ78532 C.californicum 5.8S rRNA gene and ITS1 and ITS2 DNA\n",
      "gi|2765656|emb|Z78531.1|CFZ78531 C.fasciculatum 5.8S rRNA gene and ITS1 and ITS2 DNA\n",
      "gi|2765655|emb|Z78530.1|CMZ78530 C.margaritaceum 5.8S rRNA gene and ITS1 and ITS2 DNA\n",
      "gi|2765654|emb|Z78529.1|CLZ78529 C.lichiangense 5.8S rRNA gene and ITS1 and ITS2 DNA\n"
     ]
    }
   ],
   "source": [
    "for record in SeqIO.parse(\"ls_orchid_short.fasta\", \"fasta\"):\n",
    "    print(record.description)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each time around the loop, you are given a `SeqRecord` object which will work in exactly the same way as in the previous section, so getting the `.description` works fine."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Load in the `ls_orchid_short.fasta` file and print out the length of each sequence, along with its description."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The type of the object returned by `SeqIO.parse` is known as a *generator*. One of the features of a generator in Python is that you can only loop over them once before they are *exhausted*. For example you can loop over them once without issue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['gi|2765658|emb|Z78533.1|CIZ78533', 'gi|2765657|emb|Z78532.1|CCZ78532', 'gi|2765656|emb|Z78531.1|CFZ78531', 'gi|2765655|emb|Z78530.1|CMZ78530', 'gi|2765654|emb|Z78529.1|CLZ78529']\n"
     ]
    }
   ],
   "source": [
    "records = SeqIO.parse(\"ls_orchid_short.fasta\", \"fasta\")\n",
    "print([r.id for r in records])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if you try to use the same object again, you will see that you get nothing back:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[]\n"
     ]
    }
   ],
   "source": [
    "print([r.id for r in records])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason for this is that it allows you to access, one at a time, a very long list of sequences, potentially more than can fit in memory at once since it only loads them one at a time.\n",
    "\n",
    "If you know you have a short set of sequences (as we have in this tutorial) then you can load them all into a Python list and access them however you wish:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['gi|2765658|emb|Z78533.1|CIZ78533', 'gi|2765657|emb|Z78532.1|CCZ78532', 'gi|2765656|emb|Z78531.1|CFZ78531', 'gi|2765655|emb|Z78530.1|CMZ78530', 'gi|2765654|emb|Z78529.1|CLZ78529']\n"
     ]
    }
   ],
   "source": [
    "records = list(SeqIO.parse(\"ls_orchid_short.fasta\", \"fasta\"))\n",
    "print([r.id for r in records])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['gi|2765658|emb|Z78533.1|CIZ78533', 'gi|2765657|emb|Z78532.1|CCZ78532', 'gi|2765656|emb|Z78531.1|CFZ78531', 'gi|2765655|emb|Z78530.1|CMZ78530', 'gi|2765654|emb|Z78529.1|CLZ78529']\n"
     ]
    }
   ],
   "source": [
    "print([r.id for r in records])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Genbank files\n",
    "\n",
    "As well as FASTA files, Biopython can read GenBank files. All you need to do is specify the filetype when calling the `SeqIO.parse` function. If you pass `\"genbank\"` (or `\"gb\"`) as the second argument then it will read it as a GenBankfile:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "record_iterator = SeqIO.parse(\"ls_orchid.gbk\", \"genbank\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are loading a file with multiple sequences in, you can grab just the first one with Python's [`next`](https://docs.python.org/3/library/functions.html#next) function. This gives you whatever would be available the first time around the loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "first_record = next(record_iterator)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "GenBank fiiles usually contain a lot more information than a FASTA so more of the fields of the `SeqRecord` will be filled in. This means that we can, for example, see the annotations that the sequecne has:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'molecule_type': 'DNA',\n",
       " 'topology': 'linear',\n",
       " 'data_file_division': 'PLN',\n",
       " 'date': '30-NOV-2006',\n",
       " 'accessions': ['Z78533'],\n",
       " 'sequence_version': 1,\n",
       " 'gi': '2765658',\n",
       " 'keywords': ['5.8S ribosomal RNA',\n",
       "  '5.8S rRNA gene',\n",
       "  'internal transcribed spacer',\n",
       "  'ITS1',\n",
       "  'ITS2'],\n",
       " 'source': 'Cypripedium irapeanum',\n",
       " 'organism': 'Cypripedium irapeanum',\n",
       " 'taxonomy': ['Eukaryota',\n",
       "  'Viridiplantae',\n",
       "  'Streptophyta',\n",
       "  'Embryophyta',\n",
       "  'Tracheophyta',\n",
       "  'Spermatophyta',\n",
       "  'Magnoliophyta',\n",
       "  'Liliopsida',\n",
       "  'Asparagales',\n",
       "  'Orchidaceae',\n",
       "  'Cypripedioideae',\n",
       "  'Cypripedium'],\n",
       " 'references': [Reference(title='Phylogenetics of the slipper orchids (Cypripedioideae: Orchidaceae): nuclear rDNA ITS sequences', ...),\n",
       "  Reference(title='Direct Submission', ...)]}"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "first_record.annotations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Take a look at the record and see what other `SeqRecord` attributes are filled in which were missing from the FASTA file we loaded earlier."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output\n",
    "\n",
    "We’ve talked about using `Bio.SeqIO.parse` for sequence input (reading files), and now we’ll look at `Bio.SeqIO.write` which is for sequence output (writing files). This is a function taking three arguments: some `SeqRecord` objects, a handle or filename to write to, and a sequence format.\n",
    "\n",
    "Here is an example, where we start by creating a few `SeqRecord` objects the hard way (by hand, rather than by loading them from a file):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "from Bio.Seq import Seq\n",
    "from Bio.SeqRecord import SeqRecord\n",
    "\n",
    "rec1 = SeqRecord(\n",
    "    Seq(\n",
    "        \"MMYQQGCFAGGTVLRLAKDLAENNRGARVLVVCSEITAVTFRGPSETHLDSMVGQALFGD\"\n",
    "        \"GAGAVIVGSDPDLSVERPLYELVWTGATLLPDSEGAIDGHLREVGLTFHLLKDVPGLISK\"\n",
    "        \"NIEKSLKEAFTPLGISDWNSTFWIAHPGGPAILDQVEAKLGLKEEKMRATREVLSEYGNM\"\n",
    "        \"SSAC\",\n",
    "    ),\n",
    "    id=\"gi|14150838|gb|AAK54648.1|AF376133_1\",\n",
    "    description=\"chalcone synthase [Cucumis sativus]\",\n",
    ")\n",
    "\n",
    "rec2 = SeqRecord(\n",
    "    Seq(\n",
    "        \"YPDYYFRITNREHKAELKEKFQRMCDKSMIKKRYMYLTEEILKENPSMCEYMAPSLDARQ\"\n",
    "        \"DMVVVEIPKLGKEAAVKAIKEWGQ\",\n",
    "    ),\n",
    "    id=\"gi|13919613|gb|AAK33142.1|\",\n",
    "    description=\"chalcone synthase [Fragaria vesca subsp. bracteata]\",\n",
    ")\n",
    "\n",
    "my_records = [rec1, rec2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a list of `SeqRecord` objects, we’ll write them to a FASTA format file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "SeqIO.write(my_records, \"my_example.faa\", \"fasta\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `2` that gets returned tells you how many records were written."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Create a `SeqRecord` by hand and write it to a `.fasta` file. Then read that file in and check that the details match."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
