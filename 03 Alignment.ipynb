{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multiple Alignment Sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple Sequence Alignments are a collection of multiple sequences which have been aligned together, usually with the insertion of gap characters, such that all the sequence strings are the same length. Alignment can be regarded as a matrix of letters, where each row is held as a `SeqRecord` object internally.\n",
    "\n",
    "The `MultipleSeqAlignment` object holds this kind of data, and the `AlignIO` module is used for reading and writing them as various file formats.\n",
    "\n",
    "[The detail API of the `AlignIO` module](http://biopython.org/DIST/docs/api/Bio.AlignIO-module.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Files for this chapter\n",
    "\n",
    "For this section you need to download some files which we will be reading in. You can either download them by hand from using these links: [PF05371_seed.sth](https://milliams.com/courses/biopython/PF05371_seed.sth) and [dummy_aln.phy](https://milliams.com/courses/biopython/dummy_aln.phy) or run the following Python code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import urllib\n",
    "for f in [\"PF05371_seed.sth\", \"dummy_aln.phy\"]:\n",
    "    urllib.request.urlretrieve(f\"https://milliams.com/courses/biopython/{f}\", f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parsing or Reading Sequence Alignments\n",
    "Much like `SeqIO`, `AlignIO` contains 2 functions for reading in sequence alignments:\n",
    "\n",
    "- `read()` - will return a single `MultipleSeqAlignment` object\n",
    "- `parse()` - will return an iterator which gives `MultipleSeqAlignment` objects\n",
    "\n",
    "Both functions expect two mandatory arguments:\n",
    "\n",
    "- A string specifying a handle to an open file or a filename.\n",
    "- A lower case string specifying the alignment format [See here for a full listing of supported formats](http://biopython.org/wiki/AlignIO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single alignments\n",
    "\n",
    "Let's start with a single alignments file which contains the seed alignment for the Phage_Coat_Gp8 (PF05371) PFAM entry. The file contains a lot of annotation information but let's just go ahead and load it in to see how it looks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Alignment with 7 rows and 52 columns\n",
      "AEPNAATNYATEAMDSLKTQAIDLISQTWPVVTTVVVAGLVIRL...SKA COATB_BPIKE/30-81\n",
      "AEPNAATNYATEAMDSLKTQAIDLISQTWPVVTTVVVAGLVIKL...SRA Q9T0Q8_BPIKE/1-52\n",
      "DGTSTATSYATEAMNSLKTQATDLIDQTWPVVTSVAVAGLAIRL...SKA COATB_BPI22/32-83\n",
      "AEGDDP---AKAAFNSLQASATEYIGYAWAMVVVIVGATIGIKL...SKA COATB_BPM13/24-72\n",
      "AEGDDP---AKAAFDSLQASATEYIGYAWAMVVVIVGATIGIKL...SKA COATB_BPZJ2/1-49\n",
      "AEGDDP---AKAAFDSLQASATEYIGYAWAMVVVIVGATIGIKL...SKA Q9T0Q9_BPFD/1-49\n",
      "FAADDATSQAKAAFDSLTAQATEMSGYAWALVVLVVGATVGIKL...SRA COATB_BPIF1/22-73\n"
     ]
    }
   ],
   "source": [
    "from Bio import AlignIO\n",
    "aln_seed = AlignIO.read(\"PF05371_seed.sth\", \"stockholm\")\n",
    "print(aln_seed)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note in the above output the sequences have been elided in the middle (`...`). We could instead write our own code to format this as we please by iterating over the rows as `SeqRecord` objects and printing the first 50 values of each sequence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "AEPNAATNYATEAMDSLKTQAIDLISQTWPVVTTVVVAGLVIRLFKKFSS - COATB_BPIKE/30-81\n",
      "AEPNAATNYATEAMDSLKTQAIDLISQTWPVVTTVVVAGLVIKLFKKFVS - Q9T0Q8_BPIKE/1-52\n",
      "DGTSTATSYATEAMNSLKTQATDLIDQTWPVVTSVAVAGLAIRLFKKFSS - COATB_BPI22/32-83\n",
      "AEGDDP---AKAAFNSLQASATEYIGYAWAMVVVIVGATIGIKLFKKFTS - COATB_BPM13/24-72\n",
      "AEGDDP---AKAAFDSLQASATEYIGYAWAMVVVIVGATIGIKLFKKFAS - COATB_BPZJ2/1-49\n",
      "AEGDDP---AKAAFDSLQASATEYIGYAWAMVVVIVGATIGIKLFKKFTS - Q9T0Q9_BPFD/1-49\n",
      "FAADDATSQAKAAFDSLTAQATEMSGYAWALVVLVVGATVGIKLFKKFVS - COATB_BPIF1/22-73\n"
     ]
    }
   ],
   "source": [
    "for record in aln_seed:\n",
    "    print(f\"{record.seq[:50]} - {record.id}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With any supported file format, we can load an alignment in exactly the same way just by changing the format string. For example, use `\"phylip\"` for PHYLIP files, `\"nexus\"` for NEXUS files or `\"emboss\"` for the alignments output by the EMBOSS tools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiple Alignments\n",
    "\n",
    "In general alignment files can contain multiples alignments, and to read these files we must use the `AlignIO.parse` function.\n",
    "\n",
    "We have previously downloaded a file called `dummy_aln.phy` which contains some dummy alignment information in PHYLIP format. If we wanted to read this in using `AlignIO` we could use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Alignment with 5 rows and 6 columns\n",
      "AAACCA Alpha\n",
      "AAACCC Beta\n",
      "ACCCCA Gamma\n",
      "CCCAAC Delta\n",
      "CCCAAA Epsilon\n",
      "---\n",
      "Alignment with 5 rows and 6 columns\n",
      "AAACAA Alpha\n",
      "AAACCC Beta\n",
      "ACCCAA Gamma\n",
      "CCCACC Delta\n",
      "CCCAAA Epsilon\n",
      "---\n",
      "Alignment with 5 rows and 6 columns\n",
      "AAAAAC Alpha\n",
      "AAACCC Beta\n",
      "AACAAC Gamma\n",
      "CCCCCA Delta\n",
      "CCCAAC Epsilon\n",
      "---\n"
     ]
    }
   ],
   "source": [
    "aln_dummy = AlignIO.parse(\"dummy_aln.phy\", \"phylip\")\n",
    "for alignment in aln_dummy:\n",
    "    print(alignment)\n",
    "    print(\"---\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.parse()` function returns an iterator. If we want to keep all the alignments in memory at once, then we need to turn the iterator into a list, just as we did with `SeqIO.parse`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Alignment with 5 rows and 6 columns\n",
      "AAACAA Alpha\n",
      "AAACCC Beta\n",
      "ACCCAA Gamma\n",
      "CCCACC Delta\n",
      "CCCAAA Epsilon\n"
     ]
    }
   ],
   "source": [
    "alignments = list(AlignIO.parse(\"dummy_aln.phy\", \"phylip\"))\n",
    "second_aln = alignments[1]\n",
    "print(second_aln)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Writing Alignments\n",
    "\n",
    "Now we’ll look at `AlignIO.write()` which is for alignments output (writing files). \n",
    "\n",
    "This function takes 3 arguments: \n",
    "- Some `MultipleSeqAlignment` objects \n",
    "- A string specifying a handle or a filename to write to\n",
    "- A lower case string specifying the sequence format.\n",
    "\n",
    "We start by creating a `MultipleSeqAlignment` object the hard way (by hand). Note we create some `SeqRecord` objects to construct the alignment from."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Alignment with 3 rows and 12 columns\n",
      "ACTGCTAGCTAG toto\n",
      "ACT-CTAGCTAG titi\n",
      "ACTGCTAGDTAG tata\n"
     ]
    }
   ],
   "source": [
    "from Bio.Align import MultipleSeqAlignment\n",
    "\n",
    "from Bio.Seq import Seq\n",
    "from Bio.SeqRecord import SeqRecord\n",
    "\n",
    "align1 = MultipleSeqAlignment([\n",
    "    SeqRecord(Seq(\"ACTGCTAGCTAG\"), id=\"toto\"),\n",
    "    SeqRecord(Seq(\"ACT-CTAGCTAG\"), id=\"titi\"),\n",
    "    SeqRecord(Seq(\"ACTGCTAGDTAG\"), id=\"tata\"),\n",
    "])\n",
    "\n",
    "print(align1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try to output, in PHYLIP format, these alignments in a file with the Phage_Coat_Gp8 alignments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_alignments = [align1, aln_seed]\n",
    "AlignIO.write(my_alignments, \"mixed.phy\", \"phylip\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Read in the alignment in `PF05371_seed.sth` and write it out in PHYLIP format."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## External tools\n",
    "\n",
    "Biopython also has the ability to call out to lots of different external alignment tools including ClustalW, MUSCLE, EMBOSS, DIALIGN2-2, TCoffee and MSAProbs. Have a look at the classes [in `Bio.Align.Applications` for more details](https://biopython.org/docs/latest/api/Bio.Align.Applications.html).\n",
    "\n",
    "By using the Biopython interfaces to these tools, you can build full pipelines in Python, making use of whatever tool is best for the particular job you want to do."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
