{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Online databases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is a lot more information in the [official documentation](https://biopython.org/DIST/docs/tutorial/Tutorial.html#sec139). What follows here is a summary of the key points so that you can start using the online databases.\n",
    "\n",
    "The `Bio.Entrez` module give you Python access to the NCBI's online databases. Before you start using it, there are a few things to be aware of:\n",
    "\n",
    "- For any series of more than 100 requests, do this at weekends or outside USA peak times. This is up to you to obey.\n",
    "- Use the `email` parameter so the NCBI can contact you if there is a problem. You can set a global email address:\n",
    "  ```python\n",
    "  from Bio import Entrez\n",
    "  Entrez.email = \"A.N.Other@example.com\"\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## EInfo - Obtaining information about the Entrez databases\n",
    "\n",
    "Let's start with the EInfo utility. This allows you to ask the service for its list of available databases:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['pubmed', 'protein', 'nuccore', 'ipg', 'nucleotide', 'structure', 'genome', 'annotinfo', 'assembly', 'bioproject', 'biosample', 'blastdbinfo', 'books', 'cdd', 'clinvar', 'gap', 'gapplus', 'grasp', 'dbvar', 'gene', 'gds', 'geoprofiles', 'homologene', 'medgen', 'mesh', 'nlmcatalog', 'omim', 'orgtrack', 'pmc', 'popset', 'proteinclusters', 'pcassay', 'protfam', 'pccompound', 'pcsubstance', 'seqannot', 'snp', 'sra', 'taxonomy', 'biocollections', 'gtr']\n"
     ]
    }
   ],
   "source": [
    "from Bio import Entrez\n",
    "Entrez.email = \"matt.williams@bristol.ac.uk\"  # Always tell NCBI who you are\n",
    "\n",
    "handle = Entrez.einfo()\n",
    "result = Entrez.read(handle)\n",
    "print(result[\"DbList\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So we can see, for example, the \"pubmed\", \"protein\" and \"structure\" databases.\n",
    "\n",
    "You can delve into any one of the databases individually by passing the name of it to the `einfo` function. For example, you can ask it for the valid list of query fields which we will be able to use shortly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "ALL All terms from all searchable fields\n",
      "UID Unique number assigned to each sequence\n",
      "FILT Limits the records\n",
      "WORD Free text associated with record\n",
      "TITL Words in definition line\n",
      "KYWD Nonstandardized terms provided by submitter\n",
      "AUTH Author(s) of publication\n",
      "JOUR Journal abbreviation of publication\n",
      "VOL Volume number of publication\n",
      "ISS Issue number of publication\n",
      "PAGE Page number(s) of publication\n",
      "ORGN Scientific and common names of organism, and all higher levels of taxonomy\n",
      "ACCN Accession number of sequence\n",
      "PACC Does not include retired secondary accessions\n",
      "GENE Name of gene associated with sequence\n",
      "PROT Name of protein associated with sequence\n",
      "ECNO EC number for enzyme or CAS registry number\n",
      "PDAT Date sequence added to GenBank\n",
      "MDAT Date of last update\n",
      "SUBS CAS chemical name or MEDLINE Substance Name\n",
      "PROP Classification by source qualifiers and molecule type\n",
      "SQID String identifier for sequence\n",
      "GPRJ BioProject\n",
      "SLEN Length of sequence\n",
      "FKEY Feature annotated on sequence\n",
      "PORG Scientific and common names of primary organism, and all higher levels of taxonomy\n",
      "COMP Component accessions for an assembly\n",
      "ASSM Assembly\n",
      "DIV Division\n",
      "STRN Strain\n",
      "ISOL Isolate\n",
      "CULT Cultivar\n",
      "BRD Breed\n",
      "BIOS BioSample\n"
     ]
    }
   ],
   "source": [
    "handle = Entrez.einfo(db=\"nucleotide\")\n",
    "result = Entrez.read(handle)\n",
    "\n",
    "for f in result[\"DbInfo\"][\"FieldList\"]:\n",
    "    print(f[\"Name\"], f[\"Description\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 1\n",
    "\n",
    "Look at the database information for another database. What are the valid list of search fields there?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ESearch - Searching the Entrez databases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use ESearch to search GenBank. Here we’ll do a search for the *matK* gene in *Cypripedioideae* orchids. We specify the database we want to search, the search terms (based on the fields we found out about earlier) and pass the `idtype` argument to specify what we want to have returned as the `.id` field:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['OQ981989.1', 'NC_063680.1', 'NC_064145.1', 'NC_063681.1', 'NC_066405.1', 'OP465215.1', 'NC_071758.1', 'NC_069974.1', 'NC_069973.1', 'NC_069972.1', 'NC_069971.1', 'NC_069970.1', 'NC_069969.1', 'NC_069968.1', 'NC_069967.1', 'NC_069966.1', 'NC_069965.1', 'NC_069964.1', 'NC_069963.1', 'NC_069962.1']"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "handle = Entrez.esearch(db=\"nucleotide\", term=\"Cypripedioideae[Orgn] AND matK[Gene]\", idtype=\"acc\")\n",
    "record = Entrez.read(handle)\n",
    "record[\"IdList\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of the IDs (`NC_050871.1`, `NC_050870.1`, ...) is a GenBank identifier (Accession number). We'll see shortly how to actually download these GenBank records."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 2\n",
    "\n",
    "Try searching for another gene or organism and print its ID list. If you don't know of another then just try replicating the code above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## EFetch - Downloading full records from Entrez"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "EFetch is what you use when you want to retrieve a full record from Entrez. This covers several possible databases, as described on the main [EFetch help page](https://www.ncbi.nlm.nih.gov/books/NBK3837/).\n",
    "\n",
    "For most of their databases, the NCBI support several different file formats. Requesting a specific file format from Entrez using `Bio.Entrez.efetch` requires specifying the `rettype` and/or `retmode` optional arguments. The different combinations are described for each database type on the pages linked to on [NCBI efetch webpage](https://www.ncbi.nlm.nih.gov/books/NBK25499/#chapter4.EFetch).\n",
    "\n",
    "One common usage is downloading sequences in the FASTA or GenBank/GenPept plain text formats (which can then be parsed with `Bio.SeqIO`). From the Cypripedioideae example above, we can download GenBank record `NC_050871.1` using `Bio.Entrez.efetch`.\n",
    "\n",
    "We specify the database we want to fetch from, the ID returned from the search, the return type of the data (FASTA, GenBank etc.) and that we want the result as plain text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "handle = Entrez.efetch(db=\"nucleotide\", id=\"NC_050871.1\", rettype=\"gb\", retmode=\"text\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has given us a handle which we can pass to `Bio.SeqIO.read` in place of the filename:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SeqRecord(seq=Seq('GTAAAAAATCCCCATATCTTACAAGATATGGGGATTTTTTACCTTCAAAAATTC...TTT'), id='NC_050871.1', name='NC_050871', description='Paphiopedilum hirsutissimum chloroplast, complete genome', dbxrefs=['BioProject:PRJNA927338'])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from Bio import SeqIO\n",
    "SeqIO.read(handle, \"genbank\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to download the file and save a copy of it on disk, you can write the result to file with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "handle = Entrez.efetch(db=\"nucleotide\", id=\"NC_050871.1\", rettype=\"gb\", retmode=\"text\")\n",
    "record = SeqIO.read(handle, \"genbank\")\n",
    "SeqIO.write(record, \"NC_050871.gbk\", \"genbank\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 3\n",
    "\n",
    "Using one of the results from exercise 2 above, download the full record and save it to a file. Then load it from the local file using `SeqIO.read`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## EGQuery-  Global Query, counts for search terms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "EGQuery provides counts for a search term in each of the Entrez databases (i.e. a global query). This is particularly useful to find out how many items your search terms would find in each database without actually performing lots of separate searches with `ESearch`.\n",
    "\n",
    "In this example, we use `Bio.Entrez.egquery` to obtain the counts for \"Biopython\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pubmed 45\n",
      "pmc 3148\n",
      "mesh 0\n",
      "books 2\n",
      "pubmedhealth Error\n",
      "omim 0\n",
      "ncbisearch 1\n",
      "nuccore 3\n",
      "nucgss 0\n",
      "nucest 0\n",
      "protein 0\n",
      "genome 0\n",
      "structure 0\n",
      "taxonomy 0\n",
      "snp 0\n",
      "dbvar 0\n",
      "gene 0\n",
      "sra 2234\n",
      "biosystems Error\n",
      "unigene 0\n",
      "cdd 0\n",
      "clone 0\n",
      "popset 0\n",
      "geoprofiles 0\n",
      "gds 17\n",
      "homologene 0\n",
      "pccompound 0\n",
      "pcsubstance 0\n",
      "pcassay 0\n",
      "nlmcatalog 0\n",
      "probe 0\n",
      "gap 0\n",
      "proteinclusters 0\n",
      "bioproject 1\n",
      "biosample 0\n",
      "biocollections 0\n"
     ]
    }
   ],
   "source": [
    "handle = Entrez.egquery(term=\"biopython\")\n",
    "record = Entrez.read(handle)\n",
    "for row in record[\"eGQueryResult\"]:\n",
    "    print(row[\"DbName\"], row[\"Count\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise 4\n",
    "\n",
    "Run a global query for an organism name to find out how many matches there are in the various databases."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
